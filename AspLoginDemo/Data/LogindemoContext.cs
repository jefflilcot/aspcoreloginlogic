﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AspLoginDemo.Models;

namespace AspLoginDemo.Data
{
    public class LogindemoContext : DbContext
    {
        public LogindemoContext(DbContextOptions<LogindemoContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }

    }
}
