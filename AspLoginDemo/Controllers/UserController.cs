﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspLoginDemo.Data;
using AspLoginDemo.Models;

namespace AspLoginDemo.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly LogindemoContext _context;
        public UserController(LogindemoContext context)
        {
            _context = context;
        }

        // GET api/user
        [HttpGet]
        public IEnumerable<User> Get()
        
        {
            return _context.Users.ToArray();
            //return await _context.Users.ToListAsync();
            //return new string[] { "value1", "value2" };
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public IEnumerable<User> Get(int id)
        {
            return _context.Users.Where(u=>u.ID ==id);
        }

        // GET api/user/login?username=jeff&password=jefflilcot
        [HttpGet("login")]
        public IEnumerable<User> Get(string username, string password)
        {
            return _context.Users.Where(u => u.Username == username).Where(u => u.Password == password);
        }

        // GET api/user/register?username=carolmathews&email=mathews@gmail.com&password=mathews
        [HttpGet("register")]
        public IEnumerable<User> Get(string username,string email, string password)
        {
            User user = new User();
            user.Email = email;
            user.Username = username;
            user.Password = password;

            //user to db set
            _context.Users.Add(user);
                //call save changes to save to database.
                _context.SaveChanges();

            return _context.Users.ToArray();
        }
        // POST api/user
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/user/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/user/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
