﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspLoginDemo.Data;
using AspLoginDemo.Models;

namespace AspLoginDemo.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly LogindemoContext _context;
        public ValuesController(LogindemoContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<User> Get()
        
        {
            return _context.Users.ToArray();
            //return await _context.Users.ToListAsync();
            //return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // GET api/values/stringdata?identifier=8998&firstname=jeff&lastname=lilcot
        [HttpGet("stringdata")]
        public string Get(int id,string firstname, string lastname)
        {
            return "value-with "+id+" "+firstname+" "+lastname;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
